<?php

namespace App\Http\Middleware;
use Closure;
use Session;

class SessionCheck
{
    public function handle($request, Closure $next)
    {
        if(Session::has('admin_id')) 
        {
            return $next($request);
        }else{
            return redirect('/')->with("status_fail", "Your session is expired. Please login again.");
        }
    }
}
