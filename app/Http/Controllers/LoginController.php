<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Session;

class LoginController extends Controller
{
	public function ViewLogin(){
		return view('login/login');
	}

	public function Login(Request $req){
		$validation = $req->validate([
			'username' => 'required',
			'password' => 'required'
		]);
		if($validation == true){
			$user = DB::table('ms_admin')->where('username', $req->input('username'))
			->where('password', md5($req->input('password')))
			->where('status', 'active')
			->first();
			if($user){
				session(['admin_id' => $user->id, 'admin_name' => $user->name]);
				return redirect('/slider');
			} else {
				return redirect('/')->with("status_fail", "Login Failed. Username or Password is incorrect.");
			}
		} else {
			return redirect('/')->with("status_fail", "Login Failed. Please fill your username and password.");
		}
	}

	public function Register(Request $req){
		$validation = $req->validate([
			'username' => 'required|email',
			'name' => 'required',
			'password' => 'required',
			'rpassword' => 'required'
		]);
		if($validation == true){
			if($req->input('password') == $req->input('rpassword')){
				$check = DB::table('ms_admin')->where('username', $req->input('username'))->first();
				if($check){
					$return["Status"] = "failed";
					$return["Message"] = "Your username already registered. Please contact our support.";
				} else {
					$array_input['username'] = $req->input('username');
					$array_input['name'] = $req->input('name');
					$array_input['password'] = md5($req->input('password'));
					$array_input['status'] = 'active';
					$array_input['created_at'] = Carbon::now();
					$array_input['created_by'] = 0;
					if(DB::table('ms_admin')->insert($array_input)){
						$return['Status'] = "success";
						$return['Message'] = "Success register.";
					} else {
						$return['Status'] = "failed";
						$return['Message'] = "Something went wrong. Internal server error (500).";
					}
				}
			} else {
				$return['Status'] = "failed";
				$return['Message'] = "Please enter the same password.";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Please check your data. All field is required.";
		}
		return $return;
	}

	public function ForgotPassword(Request $req){
		$validation = $req->validate([
			'email' => 'required'
		]);
		if($validation == true){
			$check = DB::table('ms_admin')
			->where('username', $req->input('email'))
			->first();
			if($check){
				try{
					DB::table('ms_admin')
					->where('username', $req->input('email'))
					->update(['password' => md5('123')]);
				}
				catch(Exception $e){
					$return['Status'] = "failed";
					$return['Message'] = "Something went wrong. Please contact our support.";
				}
				$return['Status'] = "success";
				$return['Message'] = "Your new password is 123.";
			} else {
				$return['Status'] = "failed";
				$return['Message'] = "Username not found.";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Please check your data. All field is required.";
		}
		return $return;
	}

	public function CheckSession(){
		return Session::all();
	}
}
;