<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class EmployeeController extends Controller
{
	private function CountWorkingDays($year, $month){
		$weekend = array(0,6);
		$count = 0;
		// $counter = Carbon::parse("01-".$month."-".$year);
		$counter = mktime(0, 0, 0, $month, 1, $year);
		while(date("n", $counter) == $month){
			if(in_array(date("w", $counter), $weekend) == false){
				$count++;
			}
			// $counter = Carbon::parse($counter)->addDay();
			$counter = strtotime("+1 day", $counter);
		}
		return $count;
	}
	public function ViewRegister(){
		$gol = DB::table('ms_golongan')->where('deleted_at', null)->get();
		return view('employee/register')
		->with('gol', $gol);
	}

	public function CreateEmployee(Request $req){
		$validation = $req->validate([
			'nik' => 'required',
			'nama' => 'required',
			'alamat' => 'required',
			'no_id' => 'required',
			'golongan' => 'required',
			'gaji_pokok' => 'required',
			'user_id' => 'required'
		]);
		if($validation == true){
			$array_input['nik'] = $req->input('nik');
			$array_input['nama'] = $req->input('nama');
			$array_input['alamat'] = $req->input('alamat');
			$array_input['nomor_identitas'] = $req->input('no_id');
			$array_input['golongan'] = $req->input('golongan');
			$array_input['gaji_pokok'] = $req->input('gaji_pokok');
			$array_input['created_at'] = Carbon::now();
			$array_input['created_by'] = $req->input('user_id');
			if(DB::table('ms_karyawan')->insert($array_input)){
				$return['Status'] = "success";
				$return['Message'] = "Success register.";
			} else {
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Internal server error (500).";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Please check your data. All Field are required";
		}
		return $return;
	}

	public function ViewRegisterHoliday(){
		return view('employee/register-holiday');
	}

	public function RegisterHoliday(Request $req){
		$validation = $req->validate([
			'holiday' => 'required',
			'user_id' => 'required'
		]);
		if($validation == true){
			$nameofday = Carbon::parse($req->input('holiday'))->format('l');
			$check = DB::table('ms_libur')->where('date', Carbon::parse($req->input('holiday')))->first();
			if($nameofday == "Saturday" || $nameofday == "Sunday"){
				$return["Status"] = "failed";
				$return["Message"] = "Date is weekend, please choose correct date.";
			} else if($check){
				$return["Status"] = "failed";
				$return["Message"] = "Date already submitted to database.";
			} else {
				$array_input["date"] = Carbon::parse($req->input('holiday'));
				$array_input["created_at"] = Carbon::now();
				$array_input["created_by"] = $req->input('user_id');
				if(DB::table('ms_libur')->insert($array_input)){
					$return['Status'] = "success";
					$return['Message'] = "Success register date.";
				} else {
					$return['Status'] = "failed";
					$return['Message'] = "Something went wrong. Internal server error (500).";
				}
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Please check your data. All field is required.";
		}
		return $return;
	}

	public function ViewSalary(){
		return view('employee/salary');
	}

	public function CheckNIK(Request $req){
		$validation = $req->validate([
			'nik_search' => 'required'
		]);
		if($validation == true){
			$data_karyawan = DB::table('ms_karyawan')
			->join('ms_golongan', 'ms_karyawan.golongan', '=', 'ms_golongan.id')
			->where('ms_karyawan.nik', $req->input('nik_search'))
			->where('ms_karyawan.status', 'active')
			->select('ms_karyawan.id','ms_karyawan.nik','ms_karyawan.nama', 'ms_karyawan.golongan', 'ms_golongan.nama as nama_golongan', 'ms_karyawan.gaji_pokok')
			->first();
			if($data_karyawan){
				$return['Status'] = "success";
				$return['Data'] = (array)$data_karyawan;
			} else {
				$return['Status'] = "failed";
				$return['Message'] = "NIK not found. Please insert other NIK.";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Please check your data. All field is required.";
		}
		return $return;
	}

	public function CheckDate(Request $req){
		$validation = $req->validate([
			'date' => 'required',
			'id_karyawan' => 'required'
		]);
		if($validation == true){
			$array_date = explode("-", $req->input('date'));
			$check = DB::table('tr_gaji')
			->where('id_karyawan', $req->input('id_karyawan'))
			->whereMonth('date_gaji', '=', $array_date[0])
			->whereYear('date_gaji', '=', $array_date[1])
			->first();
			if($check){
				$return['Status'] = "failed";
				$return['Message'] = "Salary for the selected month already submitted. Please choose another month.";
			} else {
				$return['Status'] = "success";
				$return['Data'] = [
					"MaxDay" => Carbon::parse("01-".$req->input('date'))->endOfMonth()->format('t')
				];
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please refresh this page.";
		}
		return $return;
	}

	public function Calculate(Request $req){
		$validation = $req->validate([
			"id_karyawan" => 'required',
			"id_golongan" => 'required',
			"gaji_pokok" => 'required',
			'date' => 'required',
			"hadir" => 'required',
			"telat" => 'required'
		]);
		if($validation == true){
			try{
				$array_date = explode("-", $req->input('date'));
				$normal_working_day = $this->CountWorkingDays($array_date[1], $array_date[0]);
				$weekend_day = Carbon::parse('01-'.$req->input('date'))->endOfMonth()->format('t') - $normal_working_day;
				$holiday = DB::table('ms_libur')
				->whereMonth('date', '=', $array_date[0])
				->whereYear('date', '=', $array_date[1])
				->count();
				$working_day = $normal_working_day - $holiday;
				$gaji_golongan = DB::table('ms_golongan')->where('id', $req->input('id_golongan'))->first();
				$bonus_gaji = 0;
				if($req->input('hadir') > $working_day){
					$bonus_gaji = 300000;
					$lembur = $req->input('hadir') - $working_day;
					$bonus_gaji = ($working_day * $gaji_golongan->amount);
					if($lembur > $weekend_day){
						$work_holiday = $lembur - $weekend_day;
						$bonus_gaji += $weekend_day * 2 * $gaji_golongan->amount;
						$bonus_gaji += $work_holiday * 2.5 * $gaji_golongan->amount;
					}
				}
				$total_gaji = $req->input('gaji_pokok') + $bonus_gaji;
				if($req->input('telat') > 60){
					$total_gaji = $total_gaji - ($req->input('telat') * 2000);
				}
			}
			catch(Exception $e){
				$return['Status'] = 'failed';
				$return['Message'] = "Something went wrong. Please contact our support.";
				return $return;
			}
			$return['Status'] = 'success';
			$return['Data'] = [
				"TotalSalary" => $total_gaji,
			];
		} else {
			$return['Status'] = 'failed';
			$return['Message'] = "Something went wrong. Please refresh this page.";
		}
		return $return;
	}
	public function VerificateSalary(Request $req){
		$validation = $req->validate([
			"id_karyawan" => 'required',
			"id_golongan" => 'required',
			'date' => 'required',
			"hadir" => 'required',
			"telat" => 'required',
			"jumlah_gaji" => 'required',
		]);
		if($validation == true){
			$array_input["id_karyawan"] = $req->input('id_karyawan');
			$array_input["id_golongan"] = $req->input('id_golongan');
			$array_input["date_gaji"] = Carbon::parse('01-'.$req->input('date'));
			$array_input["jumlah_hadir"] = $req->input('hadir');
			$array_input["jumlah_telat"] = $req->input('telat');
			$array_input["verified"] = 1;
			$array_input["total_gaji"] = $req->input('jumlah_gaji');
			$array_input["created_at"] = Carbon::now();
			$array_input["created_by"] = $req->input('user_id');
			if(DB::table('tr_gaji')->insert($array_input)){
				$return['Status'] = 'success';
				$return['Message'] = "Success submit.";
			} else {
				$return['Status'] = 'failed';
				$return['Message'] = "Something went wrong. Please contact our support.";
			}
		} else {
			$return['Status'] = 'failed';
			$return['Message'] = "Something went wrong. Please refresh this page.";
		}
		return $return;
	}
}
