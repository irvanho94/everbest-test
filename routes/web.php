<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

try {
	DB::connection()->getPdo();
} catch (\Exception $e) {
	die("Could not connect to the database.  Please check your configuration.");
}

Route::get('/', 'LoginController@ViewLogin');
Route::post('/register', 'LoginController@Register');
Route::post('/login', 'LoginController@Login');
Route::post('/forgot_password', 'LoginController@ForgotPassword');

Route::group(['middleware' => 'SessionCheck'], function () {
	Route::get('/employee/register', 'EmployeeController@ViewRegister');
	Route::get('/employee/national_holiday', 'EmployeeController@ViewRegisterHoliday');
	Route::get('/employee/salary', 'EmployeeController@ViewSalary');
	Route::get('/slider', 'Controller@ViewSlider');
	Route::get('/responsive', 'Controller@ViewResponsive');
});

Route::post('/employee/register/create_employee', 'EmployeeController@CreateEmployee');
Route::post('/employee/national_holiday/register_holiday', 'EmployeeController@RegisterHoliday');
Route::post('/employee/salary/check', 'EmployeeController@CheckNIK');
Route::post('/employee/salary/check_date', 'EmployeeController@CheckDate');
Route::post('/employee/salary/calculate', 'EmployeeController@Calculate');
Route::post('/employee/salary/verificate', 'EmployeeController@VerificateSalary');

Route::get('/session', 'LoginController@CheckSession');