@extends("template.template")
@section('breadcrumbs')
<li>
    <span style="color: #0000EE;">Slider</span>
</li>
@endsection\
@section("active_responsive", "active")
@section("content")
  <div class="row">
    <div class="col-lg-6 col-md-6 col-xs-12">
      <img src="{{ asset('assets/global/img/portfolio/1200x900/1.jpg') }}" style="width:100%; padding-bottom: 22px;">
    </div>
    <div class="col-lg-3 col-md-3 col-xs-12">
      <img src="{{ asset('assets/global/img/portfolio/1200x900/2.jpg') }}" style="width:100%; padding-bottom: 22px;">
      <img src="{{ asset('assets/global/img/portfolio/1200x900/3.jpg') }}" style="width:100%; padding-bottom: 22px;">
    </div>
    <div class="col-lg-3 col-md-3 col-xs-12">
      <img src="{{ asset('assets/global/img/portfolio/1200x900/4.jpg') }}" style="width:100%; padding-bottom: 22px;">
      <img src="{{ asset('assets/global/img/portfolio/1200x900/5.jpg') }}" style="width:100%; padding-bottom: 22px;">
    </div>
  </div>
@endsection
@section('custom-script')
@endsection