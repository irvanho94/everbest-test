@extends("template.template")
@section('custom-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('breadcrumbs')
<li>
	<span style="color: #0000EE;">Employee</span>
</li>
<li>Salary</li>
@endsection
@section("active_employee_menu", "active open")
@section("active_employee_salary", "active")
@section("content")
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-red-sunglo">
						<span class="caption-subject bold uppercase">Search NIK</span>
					</div>
				</div>
				<div class="portlet-body form">
					<form id="check-employee" role="form">
						{{ csrf_field() }}
						<div class="form-body">
							<div class="form-group">
								<label>NIK</label>
								<input type="text" name="nik_search" id="nik_search" class="form-control" onkeypress="return Digits(event);" placeholder="Nomor Induk Karyawan">
							</div>
						</div>
						<div class="form-actions right">
							<button type="button" id="btn-search" class="btn green">Search</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-red-sunglo">
						<span class="caption-subject bold uppercase">Employee Details</span>
					</div>
				</div>
				<div class="portlet-body form">
					<form id="calculate_salary" role="form">
						{{ csrf_field() }}
						<div class="form-body">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<div class="form-group">
									<label>NIK</label>
									<input type="hidden" name="id_karyawan" id="id_karyawan">
									<input type="text" name="nik" id="nik" class="form-control" onkeypress="return Digits(event);" placeholder="Nomor Induk Karyawan" readonly>
								</div>
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Karyawan" readonly>
								</div>
								<div class="form-group">
									<label>Golongan</label>
									<input type="hidden" name="id_golongan" id="id_golongan">
									<input type="text" name="golongan" id="golongan" class="form-control" placeholder="Golongan" readonly>
								</div>
								<div class="form-group">
									<label>Gaji Pokok</label>
									<input type="text" name="gaji_pokok" id="gaji_pokok" class="form-control" onkeypress="return Digits(event);" placeholder="Gaji Pokok Karyawan" readonly>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12">
								<div class="form-group">
									<label>Select Month and Year</label>
									<div class="input-group input-medium date date-picker" data-date="{{ Carbon\Carbon::now()->format('m/Y') }}" data-date-format="mm-yyyy" data-date-viewmode="years" data-date-minviewmode="months">
										<input type="text" class="form-control" name="date" id="date" readonly>
										<span class="input-group-btn">
											<button class="btn default" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="form-group">
									<label>Jumlah Hadir</label>
									<input type="number" name="hadir" id="hadir" class="form-control" placeholder="Jumlah Hadir" onkeypress="return Digits(event);">
								</div>
								<div class="form-group">
									<label>Jumlah Keterlambatan</label>
									<input type="text" name="telat" id="telat" class="form-control" placeholder="Jumlah Keterlambatan (dalam menit)" onkeypress="return Digits(event);">
								</div>
								<div class="form-group">
									<label>Jumlah Gaji</label>
									<input type="text" name="jumlah_gaji" id="jumlah_gaji" class="form-control" placeholder="Total Gaji" readonly>
								</div>
							</div>
						</div>
						<div class="form-actions right">
							<button type="button" id="btn-proses" class="btn green">Proses</button>
							<button type="button" id="btn-verifikasi" class="btn grey-salsa btn-outline" disabled><i class="fa fa-check"></i>Verifikasi</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('custom-script')
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/clockface/js/clockface.js') }}" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		$("#date").on('change', function(){
			$.ajax({
				type: "POST",
				url: "{{ url('/employee/salary/check_date') }}",
				data: { date: $('#date').val(), id_karyawan: $("#id_karyawan").val() },
				success: function(res){
					if(res.Status == "success"){
						$('#hadir').prop('disabled', false);
						$('#telat').prop('disabled', false);
						$('#btn-proses').prop('disabled', false);
						$('#btn-verifikasi').prop('disabled', false);
						$('#hadir').attr({"max": res.Data.MaxDay});
					} else {
						alertify.error(res.Message);
						$('#hadir').prop('disabled', true);
						$('#telat').prop('disabled', true);
						$('#btn-proses').prop('disabled', true);
						$('#btn-verifikasi').prop('disabled', true);
					}
				},
				error: function(res){
					alertify.error("Something went wrong. Please contact our support");
				}
			});
		});
		$("#btn-search").on('click', function(){
			var myFormData = $('#check-employee').serializeArray();
			$.ajax({
				type: "POST",
				url: "{{ url('/employee/salary/check') }}",
				data: myFormData,
				success: function(res){
					if(res.Status == "success"){
						$("#calculate_salary")[0].reset();
						var data = res.Data;
						$('#id_karyawan').val(data.id)
						$('#nik').val(data.nik);
						$('#nama').val(data.nama);
						$('#id_golongan').val(data.golongan)
						$('#golongan').val(data.nama_golongan);
						$('#gaji_pokok').val(data.gaji_pokok);
					} else {
						alertify.error(res.Message);
					}
				},
				error: function(res){
					alertify.error("Something went wrong.");
				}
			});
		});
		$('#btn-proses').on('click', function(){
			var myFormData = $("#calculate_salary").serializeArray();
			$.ajax({
				type: "POST",
				url: "{{ url('/employee/salary/calculate') }}",
				data: myFormData,
				success: function(res){
					if(res.Status == "success"){
						$('#jumlah_gaji').val(res.Data.TotalSalary);
						$('#btn-verifikasi').prop('disabled', false);
					} else {
						alertify.error(res.Message);
					}
				},
				error: function(res){
					alertify.error("Something went wrong. Please contact our support");
				}
			});
		});
		$('#btn-verifikasi').on('click', function(){
			var myFormData = $("#calculate_salary").serializeArray();
			myFormData.push({ name:"user_id", value: "{{ Session::get('admin_id') }}"});
			$.ajax({
				type: "POST",
				url: "{{ url('/employee/salary/verificate') }}",
				data: myFormData,
				success: function(res){
					if(res.Status == "success"){
						alertify.success(res.Message);
						$('#btn-verifikasi').prop('disabled', true);
						$('#btn-proses').prop('disabled', true);
					} else {
						alertify.error(res.Message);
					}
				},
				error: function(res){
					alertify.error("Something went wrong. Please contact our support");
				}
			});
		});
		function Digits(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode;
			if ((charCode >= 48 && charCode <= 57)) return true;
			return false;
		}
	</script>
@endsection