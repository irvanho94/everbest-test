@extends("template.template")
@section('custom-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('breadcrumbs')
<li>
	<span style="color: #0000EE;">Employee</span>
</li>
<li>Register Holiday</li>
@endsection
@section("active_employee_menu", "active open")
@section("active_employee_register_holiday", "active")
@section("content")
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-red-sunglo">
						<span class="caption-subject bold uppercase">National Holiday</span>
					</div>
				</div>
				<div class="portlet-body form">
					<form id="register-holiday" role="form">
						{{ csrf_field() }}
						<div class="form-body">
							<div class="form-group">
								<label class="control-label">Select Date</label>
								<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy">
									<input type="text" class="form-control" name="holiday" id="holiday" readonly>
									<span class="input-group-btn">
										<button class="btn default" type="button">
											<i class="fa fa-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-actions right">
							<button type="button" id="btn-register" class="btn green">Register</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('custom-script')
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/clockface/js/clockface.js') }}" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		$("#btn-register").on('click', function(){
			var myFormData = $("#register-holiday").serializeArray();
			myFormData.push({ name:"user_id", value: "{{ Session::get('admin_id') }}"});
			$.ajax({
				type: "POST",
				url: "{{ url('/employee/national_holiday/register_holiday') }}",
				data: myFormData,
				success: function(res){
					if(res.Status == "success"){
						alertify.success(res.Message);
						$('#holiday').val("");
					} else {
						alertify.error(res.Message);
					}
				},
				error: function(res){
					alertify.error("Something went wrong.");
				}
			});
		});
	</script>
@endsection