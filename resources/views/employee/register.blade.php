@extends("template.template")
@section('breadcrumbs')
<li>
	<span style="color: #0000EE;">Employee</span>
</li>
<li>Register</li>
@endsection
@section("active_employee_menu", "active open")
@section("active_employee_register", "active")
@section("content")
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-red-sunglo">
						<span class="caption-subject bold uppercase">Employee Register</span>
					</div>
				</div>
				<div class="portlet-body form">
					<form id="register-karyawan" role="form">
						{{ csrf_field() }}
						<div class="form-body">
							<div class="form-group">
								<label>NIK</label>
								<input type="text" name="nik" id="nik" class="form-control" onkeypress="return Digits(event);" placeholder="Nomor Induk Karyawan">
							</div>
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Karyawan">
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<textarea class="form-control" name="alamat" id="alamat"></textarea>
							</div>
							<div class="form-group">
								<label>Nomor Identitas</label>
								<input type="text" name="no_id" id="no_id" class="form-control" onkeypress="return Digits(event);" placeholder="Nomor Identitas Karyawan (KTP/SIM)">
							</div>
							<div class="form-group">
								<label>Golongan</label>
								<select name="golongan" id="golongan" class="form-control">
									<option selected disabled>Pilih Golongan</option>
									@foreach($gol as $gol_key => $data_gol)
									<option value="{{ $data_gol->id }}">{{ $data_gol->nama }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Gaji Pokok</label>
								<input type="text" name="gaji_pokok" id="gaji_pokok" class="form-control" onkeypress="return Digits(event);" placeholder="Gaji Pokok Karyawan">
							</div>
						</div>
						<div class="form-actions right">
							<button type="button" id="btn-register" class="btn green">Register</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('custom-script')
	<script>
		$("#btn-register").on('click', function(){
			var myFormData = $('#register-karyawan').serializeArray();
			myFormData.push({ name:"user_id", value: "{{ Session::get('admin_id') }}"});
			$.ajax({
				type: "POST",
				url: "{{ url('/employee/register/create_employee') }}",
				data: myFormData,
				success: function(res){
					if(res.Status == "success"){
						alertify.success(res.Message);
						$('#register-karyawan')[0].reset();
					} else {
						alertify.error(res.Message);
					}
				},
				error: function(res){
					alertify.error("Something went wrong.");
				}
			});
		});
		function Digits(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode;
			if ((charCode >= 48 && charCode <= 57)) return true;
			return false;
		}
	</script>
@endsection