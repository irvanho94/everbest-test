@extends("template.template")
@section('breadcrumbs')
<li>
    <span style="color: #0000EE;">Slider</span>
</li>
@endsection\
@section("active_slider", "active")
@section("content")
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
          <li data-target="#myCarousel" data-slide-to="4"></li>
          <li data-target="#myCarousel" data-slide-to="5"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" id="carousel-img">
          <div class="item active">
            <img src="{{ asset('assets/global/img/portfolio/1200x900/1.jpg') }}" alt="Slide 1" style=" max-height:400px; margin:auto;">
          </div>

          <div class="item">
            <img src="{{ asset('assets/global/img/portfolio/1200x900/2.jpg') }}" alt="Slide 2" style=" max-height:400px; margin:auto;">
          </div>
        
          <div class="item">
            <img src="{{ asset('assets/global/img/portfolio/1200x900/3.jpg') }}" alt="Slide 3" style=" max-height:400px; margin:auto;">
          </div>

          <div class="item">
            <img src="{{ asset('assets/global/img/portfolio/1200x900/4.jpg') }}" alt="Slide 4" style=" max-height:400px; margin:auto;">
          </div>

          <div class="item">
            <img src="{{ asset('assets/global/img/portfolio/1200x900/5.jpg') }}" alt="Slide 5" style=" max-height:400px; margin:auto;">
          </div>

          <div class="item">
            <img src="{{ asset('assets/global/img/portfolio/1200x900/6.jpg') }}" alt="Slide 6" style=" max-height:400px; margin:auto;">
          </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <div class="row" style="text-align:center; padding-top: 10px;">
          <a class="btn green" data-target="#myCarousel" data-slide-to="0">First</a>
          <a class="btn green" href="#myCarousel" data-slide="prev">Prev</a>
          <button type="button" id="btn-zoom" class="btn green">Zoom</button>
          <a class="btn green" href="#myCarousel" data-slide="next">Next</a>
          <a class="btn green" data-target="#myCarousel" data-slide-to="5">Last</a>
      </div>

<div class="modal fade" id="image-zoom" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-body" id="modal-body"></div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('custom-script')
    <script>
        $("#btn-zoom").on('click', function(){
            var child = $('#carousel-img').find(".active").children();
            $('#image-zoom').modal('show');
            $('#modal-body').html("<img src='"+child.attr('src')+"' style='width:100%;'>");
        });
    </script>
@endsection